package ru.vsu.core;

public class Repository<ID, Entity> {

    private final EntityMetaData entityMetaData;

    public Repository(EntityContext entityContextLoader, Class<Entity> typeClass) {
        this.entityMetaData = entityContextLoader.getEntityMetaData(typeClass);
    }

    public String generateSelectQuery(ID id) {
        return null;
    }


    public String generateInsertQuery(Entity entity) {
        return null;
    }

}
