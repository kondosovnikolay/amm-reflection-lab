package ru.vsu.core;

import ru.vsu.validator.ContextValidator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EntityContext {
    private final ContextValidator contextValidator;
    private final Map<Class<?>, EntityMetaData> classNameToQueryMetadata = new HashMap<>();

    public EntityContext(ContextValidator contextValidator) {
        this.contextValidator = contextValidator;
    }

    public void loadContext(Set<Class<?>> classes) {
    }

    public EntityMetaData getEntityMetaData(Class<?> entityType){
        return classNameToQueryMetadata.get(entityType);
    }

}
